﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReadClues : MonoBehaviour 
{

	public AudioClip pickUpSound;
	public AudioClip putDownSound;
	public AudioSource audioSource;
	public Image clueImage;
	public Text clueText;
	public bool isClueOpen = false;
	float clueDistance;
	public GameObject player;
	public int clueIndex;
	public float finalClue = 0f;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		audioSource = GetComponent<AudioSource> ();
	}
		
	void Update()
	{
		CheckIfRestart ();
	}

	public void OpenClue()
	{
		if (Input.GetKeyDown ("e") && CompareTag("Book") && isClueOpen == false) 
		{
			clueImage.gameObject.SetActive(true);
			audioSource.PlayOneShot (pickUpSound, 1f);
			player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ().isPaused = true;
			isClueOpen = true;

			if (clueIndex == 1) {
				clueText.text = "Dearest grandchild, I leave to you all the fortunes I have collected in life. But before you can claim them, you must play one final game with your old grandfather. In this room are three blocks that must be placed on their proper pedestals. Also in this room are clues that will help you place them correctly. And always remember-- sometimes, it is in your best interest to think outside of the box.";
			} 
			else if (clueIndex == 2) {
				clueText.text = "Cherry really likes her best friend Kiwi. They never like being far from one another, so they always sit as close to each other as they can.";
			} 
			else if (clueIndex == 3) 
			{
				clueText.text = "Blueberry wishes they could sit by Cherry at the high table, but Kiwi is always sitting by her first. Cherry wants to sit by both of her friends!";
			}
			else if (clueIndex == 4) 
			{
				clueText.text = "Unfortunately, both Kiwi and Blueberry don't like each other very much, and refuse to sit next to each other.";
			}
			else if (clueIndex == 5) 
			{
				clueText.text = "Kiwi eventually agreed to sit with Blueberry and Cherry, on the condition that she sat furthest from Blueberry, and got to pick her seat first.";
			}
			else if (clueIndex == 6) 
			{
				clueText.text = "Congradulations! You solved the puzzle! Press 'R' to restart and play again!";
				finalClue += 1f;
			}
		}
		else if (Input.GetKeyDown ("e") || Input.GetKeyDown(KeyCode.Escape)  && isClueOpen == true)
		{
			clueImage.gameObject.SetActive(false);
			audioSource.PlayOneShot (putDownSound, 1f);
			player.GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController> ().isPaused = false;
			isClueOpen = false;
		}
	}

	public void CheckIfRestart()
	{
		if (finalClue >= 1f && Input.GetKeyDown("r"))
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}
	}

}
