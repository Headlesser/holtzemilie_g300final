﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupObjects : MonoBehaviour 
{
	//determines how far the item can be thrown
	float throwForce = 600;
	public bool isHolding = false;
	float distance;
	//sets up the empty game object 'guide' to be where the block will flaot once picked up
	public Transform guide;
	public GameObject item;
	public GameObject tempParent;
	public ReadClues rc;
	public Rigidbody rb;
	public KeyPositionLogic pillar;


	//Use this for initialization
	void Start()
	{
		rb = item.GetComponent<Rigidbody> ();
		if (GetComponent<ReadClues> () != null)
		{
			rc = GetComponent<ReadClues> ();
		}
	}


	// Update is called once per frame
	void Update () {
		distance = Vector3.Distance (item.transform.position, guide.transform.position);

		if (isHolding == true) {
			
			//item.transform.position = guide.transform.position;
			rb.MovePosition(guide.transform.position);

			if (Input.GetMouseButtonDown (1)) {
				//Debug.Log ("Trying to throw");
				LetGo ();
				rb.AddForce (guide.transform.forward * throwForce);
			}
		} 
		else 
		{
			if (rc != null) 
			{
				if (distance <= 1.5f) 
				{
					rc.OpenClue ();
				}
			}
		}
	}


	void OnMouseDown()
	{
		if (distance <= 1.5f) 
		{
			isHolding = true;
			pillar.holdCheck = true;
			rb.drag = Mathf.Infinity;
			rb.angularDrag = Mathf.Infinity;

			rb.useGravity = false;
			rb.detectCollisions = true;
			rb.isKinematic = false;
			item.transform.parent = tempParent.transform;
			guide.transform.position = item.transform.position;
		}
	}


	void LetGo()
	{
		isHolding = false;
		pillar.holdCheck = false;
		rb.drag = 0f;
		rb.angularDrag = .05f;

		rb.useGravity = true;
		rb.isKinematic = false;
		item.transform.parent = null;
	}

	void OnMouseUp()
	{
		LetGo ();
	}

	//makes it so if key touches another object, it will be dropped.
	//private void OnCollisionEnter(Collision collision)
	//{
		//isHolding = false;
	//}
		
}

