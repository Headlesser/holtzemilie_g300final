﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Instructions : MonoBehaviour {

	public float startTime = 3.0f;
	public Text ruleText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		startTime -= Time.deltaTime;
		CheckTime ();
	}

	void CheckTime()
	{
		if (startTime < 0) 
		{
			ruleText.gameObject.SetActive(false);
			startTime = -1f;
		}
	}
}

 