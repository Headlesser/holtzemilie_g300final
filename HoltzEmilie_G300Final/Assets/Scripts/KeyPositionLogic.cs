﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPositionLogic : MonoBehaviour {
	public GameObject[] cubeArray;
	public bool[] cubesIn;
	public bool holdCheck;
	public GameObject wallCheck;
	public AudioClip successSound;
	public AudioSource audioSource;
	public bool wallOpen = false;
	public GameObject particles;


	// Use this for initialization
	void Start () 
	{
		audioSource = GetComponent<AudioSource>();
	}


	//just in case you can't figure it out, the answer to the puzzle is Green cube, Red cube, then Blue cube all stacked on top of each other on the pillar on top of the crate.
	// Update is called once per frame
	void Update () 
	{
		if (wallOpen == false) {
	

			if (cubesIn [1] == true && cubesIn [2] == true && cubesIn [0] == true) {
				if (cubeArray [1].transform.position.y < cubeArray [0].transform.position.y && cubeArray [0].transform.position.y < cubeArray [2].transform.position.y) {
					if (holdCheck == false) {
						wallCheck.gameObject.SetActive (false);
						particles.gameObject.SetActive (true);
						print ("played");
						audioSource.PlayOneShot (successSound, 1f);
						wallOpen = true;
					}
				}
			}

		}

	}


	//checks if the object entering the pillar's box is a cube or not.
	//if it is a cube, it sets its status to 'in the pillar'
	void OnTriggerEnter(Collider other)
	{

		for (int i = 0; i < 3; i++) 
		{
			if (other.gameObject == cubeArray [i]) 
			{
				cubesIn [i] = true;
				return;
			}
		}
	}


	//does the exact opposite of OnTriggerEnter
	void OnTriggerExit(Collider other)
	{
		for (int i = 0; i < 3; i++)
		{
			if (other.gameObject == cubeArray [i]) 
			{
				cubesIn [i] = false;
				return;
			}
		}
	}




}
